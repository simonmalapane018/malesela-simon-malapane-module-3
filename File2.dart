import 'package:flutter/material.dart'

void main(){
    runApp(My Details());
}

class My logins extends statelessWidget {
    @override
    Widget build(buildContext context) {
        return materialApp(
            title: 'Login app'
            home: LoginPage(),
        );
    }
}

class LoginPage extends statelessWidget {
    @override
    _LoginPagestate createstate()  _LoginPagestate();
}

CLASS _LoginPagestate extends State<LoginPage>{
    @override
    Widget Build(BuildContext context) {
        return Scaffold(
            body: Safefold(
                child: ListView(
                    padding: EdgeInsets.symmetric(horizontal: 18.0),
                    children: <Widget>[
                        column(
                            children: <Widget>[ 
                            SizedBox(height: 30,),
                            image.asset('assets/logo.png'),
                            SizedBox(height: 30,),
                            text('Learning material login' ,style: Textstyle(fontsize: 25,color:
                            colors.green),) 
                    ], // <Widget>[]
                        ), // column
                        SizedBox(height: 60.0,),
                        TextField(
                            decoration: inputDecoration(
                                LabelText: "Email",
                                LableStyle: Textstyle(fontsize: 20)
                                filled: true,
                            ), // inputDecoration
                        ), // TextField
                        SizedBox(height:20.0,),
                        TextField(
                            obscureText: true,
                            decoration: inputDecoration(
                                LabelText: "Password",
                                LableStyle: Textstyle(fontsize: 20)
                                filled: true,
                            ), // inputDecoration
                        ), // TextField
                        SizedBox(height:20.0,),
                        column(
                            children: <Widget>[
                                Buttontheme(
                                    height: 50,
                                    disableColor: color.blueAccent,
                                    child: RaisedButton(
                                        disabledElevation: 4.0,
                                        onPressed: null,
                                        child: Text('Login',style:Textstyle(fontsize: 20,color:
                                        Colors.white)), // textstyle // text
                                    ), // RaisedButton

                        ), // Buttontheme
                        SizedBox(height: 20,),
                        Text('New user? Sign up here')
                        ], // <Widget>[]   
                        ) // column
                    ], // <Widget>[]),
                ), // ListView
            ), // SafeArea
        ); // Scaffold
    }
}